<?php

class Service {

    private $class_name = '';
    private $authenticated = false;
    private $username;

    public function __construct($class_name) {
        $this->class_name = $class_name;
    }

    /**
     * If request contains "Security" SOAP-header, the wrapper automatically
     * checks the credentials and sets $authenticated parameter to true on success.
     * @param $header SoapHeader header
     */
    public function Security($header) {
        $this->username = $header->UsernameToken->Username;
        if ($header->UsernameToken->Username == 'TNSTestServerAccess' &&
            $header->UsernameToken->Password == '405TNS') {
            $this->authenticated = true;
        }
    }

    /**
     * Interceptor for authentication.
     * @param $method_name string method name
     * @param $parameters array array of parameters
     * @return object value of callback, or FALSE on error
     */
    public function __call($method_name, $parameters) {
        try {
            if (!$this->checkAuth()) {
                return new SoapFault("Server", "Authentication failed.");
            }
            else return call_user_func_array(array($this->class_name, $method_name), $parameters);
        }
        catch (Exception $e) { echo $e->getMessage(); };
        $this->log($this->username, $method_name, $parameters);
        return null;
    }

    private function checkAuth() {
        return $this->authenticated;
    }

    public function log($username, $method_name, $data) {
        $filename = 'log.txt';
        $handle = fopen($filename, 'a+');
        fwrite($handle, date("l dS of F Y h:i:s A") . ' - ' .
            $_SERVER['REMOTE_ADDR'] . " USER: " . $username . "\r\n" . $method_name . "\r\n" . print_r($data, true));
        fclose($handle);
    }

}

class VraceniSestavy {

    public function vrat ($arr) {
        return self::vratResponse($arr);
    }

    /**
     * Return the file.
     * @param $arr array array of parameters
     * @return object SOAP response
     */
    public function vratResponse ($arr)
    {
        $filestr = '';
        if (file_exists($arr->filename)) {
            $filestr = file_get_contents($arr->filename);
            $res['result'] = base64_encode($filestr);
        }
        else $res['result'] = null;
        return $res;
    }
    
}

$service = new Service('VraceniSestavy'); // web service instance
$server = new SOAPServer('http://localhost:80/WSDP_cacheServer_LH/VraceniSestavy.wsdl', array(
    'soap_version' => SOAP_1_2,
    'style' => SOAP_RPC,
    'use' => SOAP_LITERAL,
)); // SoapServer instance
$server->setObject($service);
$server->handle();