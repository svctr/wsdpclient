<!DOCTYPE html>
<?php include'../controller/SestavyController.php'; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sestavy</title>
    <style>
        body {
            text-align: center;
        }
        table, tr, th, td {
            border: 1px solid black;
            text-align: center;
        }
        input[name="katastrUzemiKod1"], input[name="katastrUzemiKod2"] {
            width: 25em;
        }
    </style>
</head>
<body>

<h1>Generovat list vlastnictvi</h1>

<form action="" method="post">

    <h3>List1</h3>

    <label for="katastrUzemiKod1"><b>Zadejte kod/nazev/kod obce uzemi:</b></label>
    <input list="seznamUzemi" autocomplete="off" id="katastrUzemiKod1" name="katastrUzemiKod1"/><br>

    <datalist id="seznamUzemi">
        <?php
        /** @var array $res uzemi array */
        foreach ($res as $ku) {
            echo '<option value="' . $ku->kod . '">' . $ku->kod .  ';' . $ku->nazev .  ';' . $ku->kodObce . '
        </option>';
        }
        ?>
    </datalist>

    <label for="lvCislo1"><b>Cislo listu vlastnictvi</b></label>
    <input type="text" autocomplete="off" id="lvCislo1" name="lvCislo1"><br>

    <label for="datumK1"><b>Datum</b></label>
    <input type="datetime-local" disabled="true" autocomplete="off" step="1" name="datumK1" id="datumK1" ><br>

    <label for="formatPozadavku1"><b>Format</b></label>
    <input type="text" name="formatPozadavku1" autocomplete="off" id="formatPozadavku1" placeholder="xml / pdf / html"><br><br>

    <h3>List2</h3>

    <label for="katastrUzemiKod2"><b>Kod katastru uzemi (6mistni)</b></label>
    <input type="text" autocomplete="off" name="katastrUzemiKod2" id="katastrUzemiKod2"><br>

    <label for="lvCislo2"><b>Cislo listu vlastnictvi</b></label>
    <input type="text" autocomplete="off" name="lvCislo2" id="lvCislo2"><br>

    <label for="datumK2"><b>Datum</b></label>
    <input type="datetime-local" disabled="true" autocomplete="off" step="1" name="datumK2" id="datumK2"><br>

    <label for="formatPozadavku2"><b>Format</b></label>
    <input type="text" autocomplete="off" name="formatPozadavku2" id="formatPozadavku2" placeholder=
            "xml / pdf / html"><br><br>

    <h3>Vyberte server:<br></h3>
    <input type="radio" id="kteryServer" name="kteryServer" value="serverKatastru">
    <label for="serverKatastru">server Katastru</label><br>

    <input type="radio" id="kteryServer" name="kteryServer" value="testovaciServer">
    <label for="testovaciServer">INTERNI testovaci server</label><br><br>

    <button type="submit">Generovat</button><br>

</form>


<h1>Generovat prehled vlastnictvi</h1>

<form action="" method="post">

    <label for="osobaId"><b>Zadejte id osoby:</b></label>
    <input type="text" autocomplete="off" id="osobaId" name="osobaId"><br>
    <label for="format"><b>Zadejte format:</b></label>
    <input type="text" autocomplete="off" id="format" name="format"><br><br>

    <button type="submit">Generovat</button><br><br>

</form>

<form action="" method="post">

    <h1>Odebrat sestavu</h1>
    <p>Poznamka: format musi korespondovat.</p>

    <label for="idSestavy"><b>ID</b></label>
    <input type="text" name="idSestavy" id="idSestavy"><br>

    <input type="radio" id="xml" name="formatHotovy" value="xml">
    <label for="xml">XML</label><br>

    <input type="radio" id="pdf" name="formatHotovy" value="pdf">
    <label for="pdf">PDF</label><br>

    <input type="radio" id="html" name="formatHotovy" value="html">
    <label for="html">HTML</label><br><br>

    <button type="submit">Vratit jiz generovanou sestavu</button><br><br>

</form>

<form action="" method="post">

    <h1>Listina</h1>

    <label for="idListiny"><b>ID: </b></label>
    <input type="text" name="idListiny" id="idListiny"><br><br>

    <button type="submit">Stahnout</button><br><br>

</form>

<form action="" method="post">

    <h1>Vypis z uctu</h1>

    <label for="datumOd"><b>Datum od: </b></label>
    <input type="datetime-local" autocomplete="off" step="1" name="datumOd" id="datumOd"><br>


    <input type="radio" id="xml" name="formatVypisu" value="xml">
    <label for="xml">XML</label><br>

    <input type="radio" id="pdf" name="formatVypisu" value="pdf">
    <label for="pdf">PDF</label><br>

    <input type="radio" id="html" name="formatVypisu" value="html">
    <label for="html">HTML</label><br><br>


    <button type="submit">Stahnout</button><br><br>

</form>

</body>
</html>