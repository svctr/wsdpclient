<?php

/**
 * "Sestavy" web service consumer.
 * @author Victoria Savvateeva
 */
include_once '../auth/Client.php';

class SestavyService
{
    private $client;

    public function __construct()
    {
        //$this->client = new Client("http://localhost:80/WSDP_cacheServer_LH/Server.php?wsdl", true);
        $this->client = new Client("https://katastr.cuzk.cz:443/ws/wsdp/2.8/sestavy?wsdl");
        //$this->client = new Client("https://wsdptrial.cuzk.cz:443/trial/ws/wsdp/2.8/sestavy?wsdl");
    }


    /**
     * Generates List vlastnictvi with given parameters. Outputs file in the current directory. If the format is
     * XML, print table as well.
     * @param $katastrUzemiKod integer katastralni uzemi code
     * @param $lvCislo integer List vlastnictvi's number
     * @param $format string output file format
     * @param $datumK string xs:dateTime type
     */
    public function callGenerujLVZjednodusene($katastrUzemiKod, $lvCislo, $format, $datumK = NULL)
    {
        $request_param = array(
            'katastrUzemiKod' => $katastrUzemiKod,
            'lvCislo' => $lvCislo,
            'format' => $format,
            'datumK' => '2020-08-20T09:00:00',
        );
        if (is_null($datumK) == 0) {
            $request_param['datumK'] = $datumK;
        }
        try {
            $res = $this->client->getSoapClient()->generujLVZjednodusene($request_param);
            if (gettype($res->vysledek->zprava) != 'array') {
                if ($res->vysledek->zprava->kod != 0) {
                    echo '<br>Zadal(a) jste neplatne udaje.<br>';
                    return;
                }
                else {
                    echo '<br>Pozadavek cislo ' . $res->reportList->report->id . ' v poradku odeslan.<br>';
                    sleep(6);
                    $this->callVratSestavu($res->reportList->report->id, $format);
                }
            }
            else if ($res->vysledek->zprava[0]->kod != 0) {
                echo '<br>Zadal(a) jste neplatne udaje.<br>';
                return;
            }
        } catch (SoapFault $e) {
            echo 'Soap Fault';
            echo 'Last Request: ' . $this->client->getSoapClient()->__getLastRequest();
            echo 'Error Message: ' . $e->getMessage();
            return;
        }
    }

    public function callVypisUctu($datumOd, $formatVypisu) {

        $request_param = array(
            'datumOd' => '2020-08-10T09:00:00',
            'format' => $formatVypisu,
        );
        try {
            $res = $this->client->getSoapClient()->VypisUctu($request_param);
            if (gettype($res->vysledek->zprava) != 'array') {
                if ($res->vysledek->zprava->kod != 0) {
                    echo '<br>Zadal(a) jste neplatne udaje.<br>';
                    return;
                }
                else {
                    echo '<br>Pozadavek cislo ' . $res->reportList->report->id . ' v poradku odeslan.<br>';
                    sleep(8);
                    $this->callVratSestavu($res->reportList->report->id, $formatVypisu, false,
                    true);
                }
            }
            else if ($res->vysledek->zprava[0]->kod != 0) {
                echo '<br>Zadal(a) jste neplatne udaje.<br>';
                return;
            }
        } catch (SoapFault $e) {
            echo 'Soap Fault<br>';
            echo 'Last Request: ' . $this->client->getSoapClient()->__getLastRequest() . '<br>';
            echo 'Error Message: ' . $e->getMessage();
            return;
        }

    }

    public function callGenerujPrehledVlastnictvi($osobaId, $format)
    {
        $request_param = array(
            'osId' => $osobaId,
            'format' => $format
        );

        try {
            $res = $this->client->getSoapClient()->generujPrehledVlastnictvi($request_param);
            if (gettype($res->vysledek->zprava) != 'array') {
                if ($res->vysledek->zprava->kod != 0) {
                    echo '<br>Zadal(a) jste neplatne udaje.<br>';
                    return;
                }
                else {
                    echo '<br>Pozadavek cislo ' . $res->reportList->report->id . ' v poradku odeslan.<br>';
                    sleep(8);
                    $this->callVratSestavu($res->reportList->report->id, $format, true);
                }
            }
            else if ($res->vysledek->zprava[0]->kod != 0) {
                echo '<br>Zadal(a) jste neplatne udaje.<br>';
                return;
            }
        } catch (SoapFault $e) {
            echo 'Soap Fault<br>';
            echo 'Last Request: ' . $this->client->getSoapClient()->__getLastRequest() . '<br>';
            echo 'Error Message: ' . $e->getMessage();
            return;
        }
    }

    public function callGenerujVystupZeSbirkyListin($idListiny)
    {
        $request_param = array(
            'listinaId' => $idListiny,
        );

        try {
            $res = $this->client->getSoapClient()->generujVystupZeSbirkyListin($request_param);
            var_dump($res);
            $arr = get_object_vars($res);
            echo '<pre>';
            print_r($arr);
            echo '</pre>';
            //if (gettype($res->vysledek->zprava) != 'array') {
               // if ($res->vysledek->zprava->kod != 0) {
               //     echo '<br>Zadal(a) jste neplatne udaje.<br>';
                //    return;
                //}
                //else {
                    //echo '<br>Pozadavek cislo ' . $res->reportList->report->id . ' v poradku odeslan.<br>';
                    sleep(8);
                    //$this->callVratSestavu($res->reportList->report->id, 'pdf', false);
               // }
            //}
           // else if ($res->vysledek->zprava[0]->kod != 0) {
             //   echo '<br>Zadal(a) jste neplatne udaje.<br>';
            //    return;
            //}
        } catch (SoapFault $e) {
            echo 'Soap Fault<br>';
            echo 'Last Request: ' . $this->client->getSoapClient()->__getLastRequest() . '<br>';
            echo 'Error Maessage: ' . $e->getMessage();
            return;
        }
    }

    /**
     * Outputs already existing List vlastnictvi. Outputs file in the current directory. If the format is
     * XML, prints table as well.
     * @param $idSestavy integer sestava ID
     * @param $formatSestavy string output file format - MUST correspond to the real format
     */
    public function callVratSestavu($idSestavy, $formatSestavy = null, $prehled = false, $vypis = false)
    {
        $request_param = array(
            'idSestavy' => $idSestavy,
        );
        try {
            $res = $this->client->getSoapClient()->vratSestavu($request_param);
            if (gettype($res->vysledek->zprava) != 'array') {
                if ($res->vysledek->zprava->kod != 0) {
                    echo '<br>Zadal(a) jste neplatne udaje.<br>';
                    return;
                }
            }
            else if ($res->vysledek->zprava[1]->kod != 0) {
                echo '<br>Zadal(a) jste neplatne udaje.<br>';
                return;
            }
            $filename = '';
            if ($prehled) $filename = 'PrehledVlastnictvi#';
            else if ($vypis) $filename = 'VypisUctu';
            else $filename = 'ListVlastnictvi#';
            file_put_contents($filename . $idSestavy . '.' . $formatSestavy,
                base64_decode(base64_encode($res->reportList->report->souborSestavy)));
            echo 'Soubor s nazvem ' . $filename . $idSestavy . '.' . $formatSestavy . ' je ulozen
                v aktualnim adresari.';
            if ($formatSestavy != 'xml' ) {
                echo '<br><h2>Cena sestavy: ' .$res->reportList->report->cena . '</h2><br>';
                return;
            }
            if (!$prehled) {
                $xml_str = file_get_contents($filename . $idSestavy . '.' . $formatSestavy);
                $xml = new SimpleXMLElement($xml_str);
                $this->createPozemkyTable($xml);
                echo '<br><h2>Cena sestavy: ' . $res->reportList->report->cena . '</h2><br>';
                echo '<button type="submit" name="generovatMBR">Generovat souradnice (MBR) v XML</button><br>';
            }
        } catch (SoapFault $e) {
            echo 'Soap Fault<br>';
            echo 'Last Request: ' . $this->client->getSoapClient()->__getLastRequest() . '<br>';
            echo 'Error Message: ' . $e->getMessage();
            return;
        }
    }

    /**
     * Prints table with POZEMKY data fetched from XML sestava file.
     * @param $xml object SimpleXMLElement object
     */
    private function createPozemkyTable($xml)
    {
        echo '<h2>POZEMKY</h2><br>';
        echo '<table style="width: 100%">
  <tr>
    <th>Parcela ID</th>
    <th>Zkratka</th> 
    <th>Druh. cis</th>
    <th>Par. cis</th>
    <th>Podd cis</th>
    <th>Tel.id</th> 
    <th>Vymera</th> 
    <th>Druh pozemku</th>
    <th>Zpusob vyuziti</th>
    <th>Zpusob ochrany</th> 
    <th>Soucasti</th>
    <th>Stavba na vice parcelach</th>
    <th>Soucasti stavba</th>
    <th>Stavba na parcele</th> 
    <th>Dalsi udaje</th>
    <th>Jednotky</th><form action="" method="post">';
        $i = 0;
        foreach ($xml->LIST_TEXT->TEXTY->POZEMKY->children() as $child) {
            echo '<tr>';
            foreach ($child->children() as $nextChild) {
                if ($nextChild->getName() != 'PAR_IDENT') {
                    echo '<td>' . $nextChild . '</td>';
                } else {
                    foreach ($nextChild->parcela->children() as $nextNextChild) {
                        if ($nextNextChild->getName() == 'id') {
                            echo '<td><input type="checkbox" id="parcelaId' . $i . '" name="check_list[]' . $i . '" value="' . $nextNextChild . '">
                                  <label>' . $nextNextChild . '</label><br></td>';
                            $i++;
                        } else {
                            echo '<td>' . $nextNextChild . '</td>';
                        }
                    }
                }

            }
            echo '</tr>';
        }
        echo '</table>';
    }
}