<?php
/**
 * "Informace" web service.
 * @author Victoria Savvateeva
 */

include_once '../auth/Client.php';

class InformaceService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client("https://wsdptrial.cuzk.cz:443/trial/ws/wsdp/2.8/informace?wsdl");
    }

    public function callDejMBRParcel($array) {
        $request_param = array(
            'parcelaId' => $array[0],
        );
        try {
            $res = $this->client->getSoapClient()->dejMBRParcel($request_param);
            var_dump($res);
            $arr = get_object_vars($res);
            echo '<pre>';
            print_r($arr);
            echo '</pre>';
            //file_put_contents('ListVlastnictvi#' . $idSestavy . '.' . $formatSestavy, base64_decode(base64_encode($res->reportList->report->souborSestavy)));
        }
        catch (SoapFault $e) {
            echo $this->client->getSoapClient()->__getLastResponse();
            echo $e->getMessage();
        }
    }
}