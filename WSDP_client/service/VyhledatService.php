<?php
/**
 * "Vyhledat" web service consumer.
 * @author Victoria Savvateeva
 */

include_once '../view/VyhledatVypisView.html';
include_once '../auth/Client.php';

class VyhledatService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client("https://wsdptrial.cuzk.cz:443/trial/ws/wsdp/2.8/vyhledat?wsdl");
    }

    public function callNajdiStavbu($castObceKod, $typStavbyKod, $cisloDomovni)
    {

        $request_param = array(
            'castObceKod' => $castObceKod,
            'typStavbyKod' => $typStavbyKod,
            'cisloDomovni' => $cisloDomovni,
        );
        try {
            $res = $this->client->getSoapClient()->najdiStavbu($request_param);
            echo '<pre>';
            $arr = get_object_vars ($res);
            print_r ($arr);
            if (gettype($res->vysledek->zprava) != 'array') {
                if ($res->vysledek->zprava->kod != 0) {
                    echo '<br>Zadal(a) jste neplatne udaje.<br>';
                    return;
                }
            } else if ($res->vysledek->zprava[0]->kod != 0) {
                echo '<br>Zadal(a) jste neplatne udaje.<br>';
                return;
            }

        } catch (SoapFault $e) {
            echo '<br>Zadal(a) jste neplatne udaje.<br>';
            echo $this->client->getSoapClient()->__getLastRequest();
            echo $e->getMessage();
            return;
        }
    }


    public function callNajdiJednotku($castObceKod, $typStavbyKod, $cisloDomovni)
    {

        $request_param = array(
            'castObceKod' => $castObceKod,
            'typStavbyKod' => $typStavbyKod,
            'cisloDomovni' => $cisloDomovni,
        );
        try {
            $res = $this->client->getSoapClient()->najdiJednotku($request_param);
            echo '<pre>';
            $arr = get_object_vars ($res);
            print_r ($arr);
            if (gettype($res->vysledek->zprava) != 'array') {
                if ($res->vysledek->zprava->kod != 0) {
                    echo '<br>Zadal(a) jste neplatne udaje.<br>';
                    return;
                }
            } else if ($res->vysledek->zprava[0]->kod != 0) {
                echo '<br>Zadal(a) jste neplatne udaje.<br>';
                return;
            }

        } catch (SoapFault $e) {
            echo '<br>Zadal(a) jste neplatne udaje.<br>';
            echo $this->client->getSoapClient()->__getLastRequest();
            echo $e->getMessage();
            return;
        }
    }





    public function callNajdiParcelu($kodKU, $kmenoveCislo, $poddCislo)
    {

        $request_param = array(
            'katastrUzemiKod' => $kodKU,
            'kmenoveCislo' => $kmenoveCislo,
            'poddeleni' => $poddCislo,
        );
        try {
            $res = $this->client->getSoapClient()->najdiParcelu($request_param);
            echo '<pre>';
            $arr = get_object_vars ($res);
            print_r ($arr);
            if (gettype($res->vysledek->zprava) != 'array') {
                if ($res->vysledek->zprava->kod != 0) {
                    echo '<br>Zadal(a) jste neplatne udaje.<br>';
                    return;
                }
            } else if ($res->vysledek->zprava[0]->kod != 0) {
                echo '<br>Zadal(a) jste neplatne udaje.<br>';
                return;
            }

        } catch (SoapFault $e) {
            echo '<br>Zadal(a) jste neplatne udaje.<br>';
            echo $this->client->getSoapClient()->__getLastRequest();
            echo $e->getMessage();
            return;
        }
    }
}