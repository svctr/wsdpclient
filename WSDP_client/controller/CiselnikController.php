<?php

include_once '../service/CiselnikService.php';

if(isset($_POST['seznamKU'])) {
    $cs = new CiselnikService();
    $cs->callSeznam('KU');
    $cs->printSeznamKU($cs->getSeznamUzemi());
}

else if(isset($_POST['seznamDruhuPozemku'])) {
    $cs = new CiselnikService();
    $cs->callSeznam('DruhuPozemku');
    $cs->printSeznamDruhuPozemku($cs->getSeznamDruhuPozemku());
}

else if(isset($_POST['seznamZpusobOchrany'])) {
    $cs = new CiselnikService();
    $cs->callSeznam('ZpusobOchrany');
    $cs->printSeznamZpusobOchrany($cs->getSeznamZpusobOchrany());
}

else if(isset($_POST['seznamVyuzitiPozemku'])) {
    $cs = new CiselnikService();
    $cs->callSeznam('VyuzitiPozemku');
    $cs->printSeznamZpusobVyuzitiPozemku($cs->getSeznamZpusobVyuzitiPozemku());
}

else if(isset($_POST['seznamVyuzitiJednotky'])) {
    $cs = new CiselnikService();
    $cs->callSeznam('VyuzitiJednotky');
    $cs->printSeznamZpusobVyuzitiJednotky($cs->getSeznamZpusobVyuzitiJednotky());
}

else if(isset($_POST['seznamVyuzitiStavby'])) {
    $cs = new CiselnikService();
    $cs->callSeznam('VyuzitiStavby');
    $cs->printSeznamZpusobVyuzitiStavby($cs->getSeznamZpusobVyuzitiStavby());
}

else
    if (isset($_POST['seznamTypuJednotek'])) {
        $cs = new CiselnikService();
        $cs->callSeznam('TypuJednotek');
        $cs->printSeznamTypuJednotek($cs->getSeznamTypuJednotek());
}
