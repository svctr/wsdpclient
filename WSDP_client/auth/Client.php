<?php
/**
 * SOAP Client class.
 * @author Victoria Savvateeva
 */

include_once 'WsseAuthHeader.php';

class Client
{

    private $wsse_header;
    private $wsdl;
    private $soapClient;

    /**
     * Create client.
     * @param $wsdl string a web service
     * @param $test boolean header type
     */
    public function __construct($wsdl, $test = false)
    {
        $this->wsdl = $wsdl;
        if ($test == true) {

            $this->wsse_header = new WsseAuthHeader("TNSTestServerAccess", "405TNS");
        }
        else {

            $this->wsse_header = new WsseAuthHeader("WSTEST", "WSHESLO");
           
        }
        try {

            $this->soapClient = new SoapClient($this->wsdl, array("trace" => 0, "exception" => 0,
                'cache_wsdl' => WSDL_CACHE_BOTH));
            $this->soapClient->__setSoapHeaders(array($this->wsse_header));
        } catch (SoapFault $e) {

            echo $e->getMessage() . 'Server error.<br>';
        }
    }

    /**
     * Return SoapClient.
     * @return SoapClient a SoapClient
     */
    public function getSoapClient()
    {
        return $this->soapClient;
    }
}